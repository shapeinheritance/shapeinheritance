/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeinheritance;

/**
 *
 * @author Satit Wapeetao
 */
public class Testshape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        System.out.println();
        Circle circle = new Circle(3);
        System.out.println(circle.CalArea());
        Circle circle1 = new Circle(4);
        System.out.println(circle1.CalArea());
        Triangle triangle = new Triangle(3,4);
        System.out.println(triangle.CalArea());
        Rectangle rectangle = new Rectangle(4,3);
        System.out.println(rectangle.CalArea());
        Square square = new Square(2);
        System.out.println(square.CalArea());
        System.out.println();
        
        Shape[] shapes = {circle,triangle,rectangle,square};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].CalArea());
        }
    }
}
