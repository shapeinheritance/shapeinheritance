/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeinheritance;

/**
 *
 * @author Satit Wapeetao
 */
public class Triangle extends Shape{
    private double H,B;
    public static final double area=6;
    public Triangle(double H,double B){
        System.out.println("Triangle created");
        this.H=H;
        this.B=B;
    }
    @Override
    public double CalArea(){
        return area*H*B;
    }
}
