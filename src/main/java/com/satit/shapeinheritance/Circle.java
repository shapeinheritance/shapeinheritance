/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeinheritance;

/**
 *
 * @author Satit Wapeetao
 */
public class Circle extends Shape {
    private double r;
    private static final double pi =22.0/7;
    public Circle(double r){
        this.r=r;
        System.out.println("Circle created");
    }
    @Override
    public double CalArea(){
        return pi * r *r;
    }
}
